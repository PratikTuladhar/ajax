<h1 class="text-center">AJAX CRUD</h1>
<p style="position:fixed; top:0px;left:0px;font-weight:bold;">Developed by: Pratik Tuladhar</p>
<div class="message text-center"></div>
<div class="row">
    <div class="col-md-6 col-md-offset-3">
    <button class="btn btn-primary" id="add">Add</button>
        <h3 class="text-center">List of phones</h3><br>
    <table class="table" id="table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Brand</th>
                <th>Model</th>
                
                <th>Options</th>
                
            </tr>
        </thead>
        <tbody>
            <?php $i=1;?>
            <?php foreach($lists as $list):?>

                    
                <tr data-id="<?php echo $list->id;?>" data-brand="<?php echo $list->brand;?>" data-model="<?php echo $list->model;?>" data-description="<?php echo $list->description;?>">
                    <td><?php echo $i;?></td>
                    <td><?php echo $list->brand;?></td>
                    <td><?php echo $list->model;?></td>
                    <td>
                        <a  class="btn btn-primary edit"  >Edit</a><a class="btn btn-info detail">Details</a><a class="btn btn-danger delete">Delete</a>
                    </td>
                </tr>
            <?php $i++;?>
            <?php endforeach;?>
        </tbody>
    </table>
    </div>
</div>



<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
       
        <form role="form" action="" method="post"id="form" >
            <input type="hidden" name="id" id="id">
            <div class="form-group">
                <label for="brand">Brand</label>
                <input type="text" class="form-control" id="brand" name="brand">
            </div>
            <div class="form-group">
                <label for="model">Model</label>
                <input type="text" class="form-control" id="model" name="model">
            </div>
            <div class="form-group">
                <label for="model">Description</label>
                <textarea name="description"  cols="30" rows="10" class="form-control" id="description"></textarea>
            </div>
            
        </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="submit" >Submit</button>
        <button type="button" class="btn btn-success" id="update" >Update</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script>
    
    $(document).ready( function () {
    $('#table').DataTable();
    } );
    
    function showModal(title){
        $('.modal-title').html(title);
        $("#myModal").modal('show');
    }
    
    function clearModal(){
        $('#id').val('');
        $('#brand').val('');
        $('#model').val('');
        $('#description').val('');
    }
    
    
    function disableModalElements()
    {
        $('#brand').attr('disabled','disabled');
        $('#model').attr('disabled','disabled');
        $('#description').attr('disabled','disabled');
        $('#submit').hide();
        $('#update').hide();
    }
    
    function enableModalElements()
    {
        $('#brand').removeAttr('disabled');
        $('#model').removeAttr('disabled');
        $('#description').removeAttr('disabled');
    }
    
      function detailModal(brand,model,description)
    {
        $('#brand').val(brand);
        $('#model').val(model);
        $('#description').val(description);
        disableModalElements();
        showModal('Details');
        
    }

   function editModal(id,brand,model,description)
    {
        $('#id').val(id);
        $('#brand').val(brand);
        $('#model').val(model);
        $('#description').val(description);
        enableModalElements();
        $('#update').show();
        $('#submit').hide();
        showModal('Edit');
    }
    
    $(document).on('click','#add',function(e){
       e.preventDefault();
       clearModal();
       enableModalElements();
       showModal('Add');
        $('#submit').show();
        $('#update').hide();
       
   });
    

    $(document).on('click','.edit',function(e){
        e.preventDefault();
        brand = $(this).parents('tr').data("brand");
        model = $(this).parents('tr').data("model");
        description = $(this).parents('tr').data("description");
        id = $(this).parents('tr').data("id");
        editModal(id,brand,model,description,'edit');
        $('#submit').attr('data-click','edit');
        // $('#brand').val($(this).parents('tr').data("id"));

    });
    
    $(document).on('click','.detail',function(e){
        e.preventDefault();
        brand = $(this).parents('tr').data("brand");
        model = $(this).parents('tr').data("model");
        description = $(this).parents('tr').data("description");
        detailModal(brand,model,description);
    });

    
    $(document).on('click','#submit',function(){
       
            $.ajax({
                url: "<?php echo base_url('dashboard/store');?>",
                method: "post",
                data: $('#form').serialize(),
                success: function(data){
                    
                    data = JSON.parse(data);
                    if(data.status == 'fail' )
                    {
                        
                        $('.message').html("<div class='alert alert-danger'>"+data.message+"</div>");
                        $('.message').show();
                        $('.message').fadeOut(5000);
                    
                    }
                  else{
                      $('.message').html("<div class='alert alert-success'>"+data.message+"</div>");
                      $('.message').show();
                      $('.message').fadeOut(5000);
                      $('tbody').html(data.updateTable);
                        
                  }
                    
            }
            });
   
        
     
         $('#myModal').modal('hide');
    });
    
    $(document).on('click','#update',function(){
           
           $.ajax({
           url: "<?php echo base_url('dashboard/update');?>",
           method: "post",
           data: $('#form').serialize(),
           success:function(data){
                 data = JSON.parse(data);
                    if(data.status == 'fail' )
                    {
                        
                        $('.message').html("<div class='alert alert-danger'>"+data.message+"</div>");
                        $('.message').show();
                        $('.message').fadeOut(5000);
                    
                    }
                  else{
                      $('.message').html("<div class='alert alert-success'>"+data.message+"</div>");
                      $('.message').show();
                      $('.message').fadeOut(5000);
                      $('tbody').html(data.updateTable);
                        
                  }
           }
           });
       
       
    
       $('#myModal').modal('hide');
    });

    $(document).on('click','.delete',function(){
        confirmation = confirm('Are you sure you want to delete this data');
        if (confirmation) {
            id = $(this).parents('tr').data('id');
            $.ajax({
                url: "<?php echo base_url();?>dashboard/destroy/" + id ,
                method: 'get',
                success: function(data){
                     data = JSON.parse(data);
                    if(data.status == 'fail' )
                    {
                        
                        $('.message').html("<div class='alert alert-danger'>"+data.message+"</div>");
                        $('.message').show();
                        $('.message').fadeOut(5000);
                    
                    }
                  else{
                      $('.message').html("<div class='alert alert-success'>"+data.message+"</div>");
                      $('.message').show();
                      $('.message').fadeOut(5000);
                      $('tbody').html(data.updateTable);
                        
                  }
                }
            });
        }
        else{
            return false;
        }
        
    });
    
    
   console.log("Developed by Pratik Tuladhar");
</script>