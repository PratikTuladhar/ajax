<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('general_db_model');
    }

	public function index()
	{
        $myData['lists'] = $this->general_db_model->getAll('phones');
        $myData['content'] = 'crud/list';
        $this->load->view('master',$myData);
    }

    
    
    public function store()
    {
        
        if(isset($_POST))
        {
            $config = array(
                array(
                        'field' => 'brand',
                        'label' => 'Brand',
                        'rules' => 'required'
                ),
                array(
                        'field' => 'model',
                        'label' => 'Model',
                        'rules' => 'required',
                        'errors' => array(
                                'required' => 'You must provide a %s.',
                        ),
                ),
                array(
                        'field' => 'description',
                        'label' => 'Description',
                        'rules' => 'required'
                )

                );
                 $this->form_validation->set_rules($config);
                if ($this->form_validation->run() == FALSE)
                {
                    $message = validation_errors();
                    echo json_encode(['status'=>'fail','message'=>"<div style='color:red'>$message</div>"]);
                    
                }
                else
                {
                    $info = [];
                    foreach($_POST as $index=>$row)
                    {
                        $info[$index] = $row;
                    }

                    if($this->general_db_model->insert('phones',$info))
                    {
                        $this->createTable('Record Added');

                    }
                    else
                    {
                        echo json_encode(['status'=>'fail','message'=>'<div style="color:red">The information could not be added. Please try again.</div>']);
                    }
                   
                }
      
            
        }
        
        
        
    }

    public function update()
    {
        if(isset($_POST))
        {
            $info = [];
            foreach($_POST as $index=>$row)
            {   
                if($index != "id")
                {
                    $info[$index] = $row;
                }
               
            }

            
            if($this->general_db_model->update('phones',$info,['id'=>$_POST['id']]))
            {
                $this->createTable('Record Updated');
                
            }
            else{
                echo json_encode(['status'=>'fail','message'=>'<div style="color:red">The information was not be updated which may be due to submission of same data. Please try again.</div>']);
            }
        }
    }

    public function destroy($id)
    {
        if($this->general_db_model->delete('phones',['id' =>$id ]))
        {
            $this->createTable('Record deleted');
        }
        else{
            echo json_encode(['status'=>'fail','message'=>'<div style="color:red">The information could not be deleted. Please try again.</div>']);
        }
    }

    public function createTable($message)
    {
        $data = $this->general_db_model->getAll('phones');
        $tbody = "";
        foreach($data as $row)
        {
            $tbody .= "<tr data-id='$row->id' data-brand='$row->brand' data-model='$row->model' data-description='$row->description'><td>$row->id</td><td>$row->brand</td><td>$row->model</td><td><a  class='btn btn-primary edit'  >Edit</a><a class='btn btn-info detail'>Details</a><a class='btn btn-danger delete'>Delete</a></td></tr>";
        }

        echo json_encode(['status'=>'success', 'message'=>"<div style='color:green'>".$message."</div>" , 'updateTable'=>$tbody]);
    }
}
